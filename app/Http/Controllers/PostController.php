<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    
        if($request->has('search')){
            $post = Post::where('nik', 'LIKE', '%' .$request->search. '%')
            ->orWhere('nama', 'LIKE', '%' .$request->search. '%')->paginate(5);
        } else {
            $post = Post::paginate(5);
        }

        return view('page.tabel-data', ['post' => $post]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $adp = Post::get();
        return view('page.tambah-data', ['adp' => $adp]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request -> validate([
            'nik' => 'required|unique:adp|max:16',
            'nama' => 'required',
            'umur' => 'required',
            'gender' => 'required',
            'tanggalLahir' => 'required',
            'alamat' => 'required',
            'negara' => 'required'
        ]);

        $post = new Post;

        $post->nik = $request->nik;
        $post->nama = $request->nama;
        $post->umur = $request->umur;
        $post->tanggalLahir = $request->tanggalLahir;
        $post->gender = $request->gender;
        $post->alamat = $request->alamat;
        $post->negara = $request->negara;

        $post->save();

        return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        // dd($post);
        return view('page.detail', ['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return view('page.edit', ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request -> validate([
            'nik' => 'required',
            'nama' => 'required',
            'umur' => 'required',
            'gender' => 'required',
            'tanggalLahir' => 'required',
            'alamat' => 'required',
            'negara' => 'required'
        ]);

        $post = Post::find($id);

        $post->nik = $request->nik;
        $post->nama = $request->nama;
        $post->umur = $request->umur;
        $post->tanggalLahir = $request->tanggalLahir;
        $post->gender = $request->gender;
        $post->alamat = $request->alamat;
        $post->negara = $request->negara;

        $post->save();

        return redirect('/post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        $post->delete();
        
        return redirect('/post');
    }
}
