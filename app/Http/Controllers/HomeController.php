<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function utama()
    {
        return view('page.home');
    }

    public function create()
    {
        return view('page.tambah-data');
    }

    public function store(Request $request)
    {
        $request -> validate([
            'nik' => 'required',
            'nama' => 'required',
            'umur' => 'required',
            'gender' => 'required',
            'tanggalLahir' => 'required',
            'alamat' => 'required',
            'negara' => 'required'
        ]);

        DB::table('adp')->insert([
            'nik' => $request ['nik'],
            'nama' => $request ['nama'],
            'umur' => $request ['umur'],
            'tanggalLahir' => $request ['tanggalLahir'],
            'gender' => $request ['gender'],
            'alamat' => $request ['alamat'],
            'negara' => $request ['negara']
        ]);

        return redirect('/tabel-data');
        
    }

    public function index(Request $request) {

        if($request->has('search')){
            $adp = DB::table('adp')->where('nik', 'LIKE', '%' .$request->search. '%')
            ->orWhere('nama', 'LIKE', '%' .$request->search. '%')->paginate(5);
        } else {
            $adp = DB::table('adp')->get();
        }

        // dd($adp);
        return view('page.tabel-data', ['adp' => $adp]);
    }

    public function show($id) {
        $adp = DB::table('adp')->where('id', $id)->first();

        return view('page.detail', ['adp' => $adp]);
    }

    public function edit($id) {
        $adp = DB::table('adp')->where('id', $id)->first();

        return view('page.edit', ['adp' => $adp]);
    }

    public function update(Request $request, $id) {
        $request -> validate([
            'nik' => 'required',
            'nama' => 'required',
            'umur' => 'required',
            'gender' => 'required',
            'tanggalLahir' => 'required',
            'alamat' => 'required',
            'negara' => 'required'
        ]);

        DB::table('adp')
            ->where('id', $id)
            ->update([
            'nik' => $request ->nik,
            'nama' => $request ->nama,
            'umur' => $request ->umur,
            'tanggalLahir' => $request ->tanggalLahir,
            'gender' => $request ->gender,
            'alamat' => $request ->alamat,
            'negara' => $request ->negara
        ]);

        return redirect('/tabel-data');
    }

    public function destroy($id) {
         DB::table('adp')->where('id', $id)->delete();

         return redirect('/tabel-data');
    }
}
