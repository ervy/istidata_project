@extends('master')
@section('title')
    Aplikasi Data Pribadi   
@endsection
@section('subtitle')
    Tabel Data   
@endsection
@section('content')

<!-- SidebarSearch Form -->

<label for="">NIK</label>
<div class="row g-3 align-items-center mb-2">
  <div class="col-auto">
    <form action="/post" method="GET">
      <input type="search" class="form-control" name="search">
    </form>
  </div>
</div>

<label for="">Nama</label>
<div class="row g-3 align-items-center">
  <div class="col-auto">
    <form action="/post" method="GET">
      <input type="search" class="form-control" name="search">
    </form>
  </div>
</div><br>

<a href="#" class="btn btn-primary btn-sm mb-3">Search</a>
<a href="/post/create" class="btn btn-primary btn-sm mb-3">Tambah</a>
  
  <table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">NIK</th>
        <th scope="col">Nama Lengkap</th>
        <th scope="col">Umur</th>
        <th scope="col">Tanggal lahir</th>
        <th scope="col">Jenis Kelamin</th>
        <th scope="col">Alamat</th>
        <th scope="col">Negara</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($post as $key => $value)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{$value->nik}}</td>
            <td>{{$value->nama}}</td>
            <td>{{$value->umur}}</td>
            <td>{{$value->tanggalLahir}}</td>
            <td>{{$value->gender}}</td>
            <td>{{$value->alamat}}</td>
            <td>{{$value->negara}}</td>
            <td>
              
              <a href="/post/{{$value->id}}" class="btn btn-warning btn-sm">Detail</a>
              <a href="/post/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
              <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-modal">
                Delete
              </button>
            </td>
          </tr>

          {{-- Pop up delete alert --}}
          <!-- Modal -->
          <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="exampleModalLabel"><b>Delete</b></h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  Apakah anda yakin ingin menghapus data <span><b>{{$value->nama}}</b></span> ?
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <form action="/post/{{$value->id}}" method="POST" data-toggle="modal" data-target="delete-modal">
                      @csrf
                      @method('DELETE')
                      <input type="submit" value="Delete" class="btn btn-danger">
                    </form>
                </div>
              </div>
            </div>
          </div>

      @empty
          <tr>
            <td>Tidak Ada Data</td>
          </tr>
      @endforelse
    </tbody>
  </table>

  {{ $post->links() }}
  
@endsection