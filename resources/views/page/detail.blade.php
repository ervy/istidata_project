@extends('master')
@section('title')
    Kumpulan Data    
@endsection
@section('subtitle')
    Detail Data
@endsection
@section('content')
 
<p>NIK              : {{$post->nik}}</p>
<p>Nama             : {{$post->nama}}</p>
<p>Umur             : {{$post->umur}}</p>
<p>Tanggal lahir    : {{$post->tanggalLahir}}</p>
<p>Jenis Kelamin    : {{$post->gender}}</p>
<p>Alamat           : {{$post->alamat}}</p>
<p>Negara           : {{$post->negara}}</p>

<a href="/post" class="btn btn-secondary btn-sm">Kembali</a>

@endsection