@extends('master')
@section('title')
    Aplikasi Data Pribadi
@endsection
@section('subtitle')
    Tambah Data Baru   
@endsection
@section('content')
    <form action="/post" method="POST">
        @csrf
        <label>NIK</label><br>
            <input type="number" class="form-control" name="nik"><br>
        @error('nik')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <label>Nama Lengkap</label><br>
            <input type="text" class="form-control" name="nama"><br>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <label>Tanggal lahir</label><br>
            <input id="tanggalLahir" type="date" name="tanggalLahir" class="form-control"><br>

        <label>Umur</label><br>
            <input id="umur" type="number" class="form-control" name="umur"><br>

        <label>Jenis Kelamin</label><br>
            <input type="radio" name="gender" value="Laki-Laki">  Laki-Laki<br>
            <input type="radio" name="gender" value="Perempuan">  Perempuan<br><br>

        <label>Alamat</label><br>
            <textarea name="alamat" class="form-control" rows="5"></textarea><br>   
            
        <label>Negara</label><br>
            <select name="negara" class="form-control">
                <option value="">Pilih Negara</option>
                <option value="indonesia">Indonesia</option>
                <option value="malaysia">Malaysia</option>
                <option value="singapura">Singapura</option>
                <option value="inggris">Inggris</option>
                <option value="other">Lain-lain</option>
            </select><br><br>

        <input type="submit" value="Simpan" class="btn btn-primary btn-sm">
        <a href="/" class="btn btn-warning btn-sm">Kembali</a>
    </form>

    {{-- Algoritma menghitung umur otomatis --}}
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script type="text/javascript">
    $(function(){
        $("#tanggalLahir").datapicker({
            changeMonth: true,
            changeYear: true
        });
    });
    window.onload=function(){
        $('#tanggalLahir').on('change', function(){
            var dob = new Date(this.value);
            var today = new Date();
            var age = Math.floor((today-dob)/(365.25*24*60*60*1000));
            $('#umur').val(age);
        });
    }
    </script>
    
@endsection
    