@extends('master')
@section('title')
    Aplikasi Data Pribadi
@endsection
@section('subtitle')
    Edit Data Pribadi 
@endsection
@section('content')
    <form action="/post/{{$post->id}}" method="POST">
        @csrf
        @method('PUT')
        <label>NIK</label><br>
            <input type="number" class="form-control" name="nik" value="{{$post->nik}}"><br>
        @error('nik')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <label>Nama Lengkap</label><br>
            <input type="text" class="form-control" name="nama" value="{{$post->nama}}"><br>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <label>Umur</label><br>
            <input type="number" class="form-control" name="umur" value="{{$post->umur}}"><br>

        <label>Tanggal lahir</label><br>
            <input type="date" name="tanggalLahir" class="form-control" value="{{$post->tanggalLahir}}"><br>

        <label>Jenis Kelamin</label><br>
            <input type="radio" name="gender" value="Laki-laki" {{$post->gender=='Laki-Laki'?'checked': ''}}>  Laki-Laki<br>
            <input type="radio" name="gender" value="Perempuan" {{$post->gender=='Perempuan'?'checked': ''}}>  Perempuan<br><br>

        <label>Alamat</label><br>
            <textarea name="alamat" class="form-control" rows="5">{{$post->alamat}}</textarea><br>
            
        <label>Negara</label><br>
            <select name="negara" class="form-control" value="{{$post->negara}}">
                <option value="indonesia">Indonesia</option>
                <option value="english">Malaysia</option>
                <option value="singapura">Singapura</option>
                <option value="inggris">Inggris</option>
                <option value="other">Lain-lain</option>
            </select><br><br>

        <input type="submit" value="Ubah" class="btn btn-primary btn-sm">
        <a href="/post" class="btn btn-warning btn-sm">Kembali</a>
    </form>
@endsection
    