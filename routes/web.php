<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [HomeController::class, 'utama']);

// // Create atau tambah data
// Route::get('/tambah-data', [HomeController::class, 'create']);

// // Kirim data ke database
// Route::post('/tabel-data', [HomeController::class, 'store']);

// // Tampilkan semua data
// Route::get('/tabel-data', [HomeController::class, 'index']);

// // Detail kategori berdasarkan id
// Route::get('/tabel-data/{adp_id}', [HomeController::class, 'show']);

// // Edit data di dalam form update
// Route::get('/tabel-data/{adp_id}/edit', [HomeController::class, 'edit']);

// // Update data ke database berdasarkan id
// Route::put('/tabel-data/{adp_id}', [HomeController::class, 'update']);

// // Delete
// Route::delete('/tabel-data/{adp_id}', [HomeController::class, 'destroy']);

//CRUD
Route::resource('post', PostController::class);

// search 

// latihan bilangan prima
// bilangan fibunaci angka segitiga
// pelajari logic
