<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAplikasiDataPribadiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adp', function (Blueprint $table) {
            $table->id();
            $table->unique('nik')->unique();
            $table->string('nama');
            $table->integer('umur');
            $table->date('tanggalLahir');
            $table->enum('gender',['Laki-Laki','Perempuan']);
            $table->text('alamat');
            $table->enum('negara',['Indonesia','Malaysia','Singapura','Inggris','Lain-lain']);
            $table->timestamps();
            // $table->string('Action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adp');
    }
}
